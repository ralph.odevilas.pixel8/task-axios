<?php
require_once 'MysqliDb.php';

/**
 * Tells the browser to allow code from any origin to access
 */
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE');
header('Access-Control-Allow-Headers: Content-Type');
// Identify the request method

$request_method = $_SERVER['REQUEST_METHOD'];

// For GET,POST,PUT & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];
        $request_uri = '/new_endpoint/' . $_SERVER['REQUEST_URI'];

        $ids = null;
        $exploded_request_uri = array_values(explode('/', $request_uri));

        foreach ($exploded_request_uri as $uri) {
            if (is_numeric($uri)) {
                $ids = $uri;
            }
        }
    }

    //payload data
    $received_data = json_decode(file_get_contents('php://input'), true);
}

$api = new API();

//Checking if what type of request and designating to specific functions
switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}

class API
{
    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'todos');
    }
    /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload = [])
    {
        $this->db->where('completed', $payload['completed']);
        // execute query
        $query = $this->db->get('tasks');

        //check if query is success or fail
        if ($query) {
            echo json_encode([
                'method' => 'GET',
                'status' => 'success',
                'data' => $query,
            ]);
        } else {
            echo json_encode([
                'method' => 'GET',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Fetch',
            ]);
        }
    }

    /**
     * HTTP POST Request
     *
     * @param $payload
     */
    public function httpPost($payload)
    {
        //Execute Query
        $query = $this->db->insert('tasks', $payload);

        //check if query is success or fail
        if ($query) {
            echo json_encode([
                'method' => 'POST',
                'status' => 'success',
                'data' => $payload,
            ]);
        } else {
            echo json_encode([
                'method' => 'POST',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Insert',
            ]);
        }
    }

    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {
        // where clause
        $this->db->where('id', $id);

        //execute query
        $query = $this->db->update('tasks', $payload);

        //check if query is success or fail
        if ($query) {
            echo json_encode([
                'method' => 'PUT',
                'status' => 'success',
                'data' => $payload,
            ]);
        } else {
            echo json_encode([
                'method' => 'PUT',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Update',
            ]);
        }
    }

    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id, $payload)
    {
        // Explore the ids
        $selected_id = ['id' => is_string($id) ? explode(',', $id) : null];

        //check if selected id is an array or not
        if (count($selected_id['id'])) {
            $this->db->where('id', $selected_id['id'], 'IN');
        } else {
            $this->db->where('id', $id);
        }

        // Execute query
        $query = $this->db->delete('tasks');

        // check if success or fail
        if ($query) {
            echo json_encode([
                'method' => 'DELETE',
                'status' => 'success',
                'data' => $payload,
            ]);
            return;
        } else {
            echo json_encode([
                'method' => 'DELETE',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Delete',
            ]);
        }
    }
}


?>